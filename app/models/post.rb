class Post < ActiveRecord::Base

  belongs_to :user
  has_many :comments, dependent: :destroy
  
  paginates_per 3
  
  validates :image, presence: true
  validates :user_id, presence: true
  validates :caption, presence: true, length: { minimum: 4, maximum: 150 }
  
  has_attached_file :image, styles: { :medium => "640x" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
